package com.chat.application.controller;

import com.chat.application.model.ChatMessage;
import com.chat.application.storage.StorageService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/ws")
@CrossOrigin(origins = "*", maxAge = 3600)
public class ChatController {

    private final SimpMessagingTemplate simpMessagingTemplate;
    private final StorageService storageService;

    List<String> files = new ArrayList<String>();

    public ChatController(SimpMessagingTemplate simpMessagingTemplate, StorageService storageService) {
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.storageService = storageService;
    }


    @MessageMapping("/chat.sendMessage")
    @SendTo("/topic/public")
    public ChatMessage sendMessage(@Payload ChatMessage chatMessage) {
        return chatMessage;
    }

    @MessageMapping("/chat.addUser")
    @SendTo("/topic/public")
    public ChatMessage addUser(@Payload ChatMessage chatMessage,
                               SimpMessageHeaderAccessor headerAccessor) {
        // Add username in web socket session
        headerAccessor.getSessionAttributes().put("username", chatMessage.getSender());
        return chatMessage;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> useSimpleRest(@RequestBody Map<String, String> message){
        if(message.containsKey("message")){
            //if the toId is present the message will be sent privately else broadcast it to all users
            if(message.containsKey("toId") && message.get("toId")!=null && !message.get("toId").equals("")){
                this.simpMessagingTemplate.convertAndSend("/socket-publisher/"+message.get("toId"),message);
                this.simpMessagingTemplate.convertAndSend("/socket-publisher/"+message.get("fromId"),message);
            }else{
                this.simpMessagingTemplate.convertAndSend("/socket-publisher",message);
            }
            return new ResponseEntity<>(message, new HttpHeaders(), HttpStatus.OK);
        }
        return new ResponseEntity<>(new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @MessageMapping("/send/message")
    public Map<String, String> useSocketCommunication(String message){
        ObjectMapper mapper = new ObjectMapper();
        Map<String, String> messageConvert = null;
        try {
            messageConvert = mapper.readValue(message, Map.class);
        } catch (IOException e) {
            messageConvert = null;
        }
        if(messageConvert!=null){
            if(messageConvert.containsKey("toId")
                    && messageConvert.get("toId")!=null && !messageConvert.get("toId").equals("")){
                this.simpMessagingTemplate.convertAndSend("/socket-publisher/"+messageConvert.get("toId"),messageConvert);
                this.simpMessagingTemplate.convertAndSend("/socket-publisher/"+messageConvert.get("fromId"),message);
            }else{
                this.simpMessagingTemplate.convertAndSend("/socket-publisher",messageConvert);
            }
        }
        return messageConvert;
    }

    @PostMapping(value = "/post",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> handleFileUpload(@RequestParam String message,
                                                   @RequestParam("file") MultipartFile file) {

        try {
            Object userObj = new JSONParser().parse(message);
            JSONObject userRequestJson = (JSONObject) userObj;
            String fileDownloadUri = null;
            String encodedString = null;
            String fileName = storageService.store(file);
            files.add(file.getOriginalFilename());

            if(file.getContentType().equalsIgnoreCase( "image/png") || file.getContentType().equalsIgnoreCase("image/jpeg" ) ){
                Resource resource = storageService.loadFile(fileName);
                File imageFile = resource.getFile();
                byte[] fileContent = FileUtils.readFileToByteArray(imageFile);
                encodedString = Base64
                        .getEncoder()
                        .encodeToString(fileContent);
                fileDownloadUri = resource.getURL().toString();

                userRequestJson.put("fileName",fileName);
                userRequestJson.put("fileURL",fileDownloadUri);
                userRequestJson.put("encodedString",encodedString);
                userRequestJson.put("fileSize",file.getSize()/1024+" kb");
                userRequestJson.put("fileType",file.getContentType());


                //if the toId is present the message will be sent privately else broadcast it to all users
                if(userRequestJson.containsKey("toId") && userRequestJson.get("toId")!=null && !userRequestJson.get("toId").equals("")){
                    this.simpMessagingTemplate.convertAndSend("/socket-publisher/"+userRequestJson.get("toId"),userRequestJson);
                    this.simpMessagingTemplate.convertAndSend("/socket-publisher/"+userRequestJson.get("fromId"),userRequestJson);
                }else{
                    this.simpMessagingTemplate.convertAndSend("/socket-publisher",userRequestJson);
                }
            }
            else{
                fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                        .path("/files/")
                        .path(fileName)
                        .toUriString();

                userRequestJson.put("fileName",fileName);
                userRequestJson.put("fileURL",fileDownloadUri);
                userRequestJson.put("encodedString",encodedString);
                userRequestJson.put("fileSize",file.getSize()/1024+" kb");
                userRequestJson.put("fileType",file.getContentType());


                //if the toId is present the message will be sent privately else broadcast it to all users
                if(userRequestJson.containsKey("toId") && userRequestJson.get("toId")!=null && !userRequestJson.get("toId").equals("")){
                    this.simpMessagingTemplate.convertAndSend("/socket-publisher/"+userRequestJson.get("toId"),userRequestJson);
                    this.simpMessagingTemplate.convertAndSend("/socket-publisher/"+userRequestJson.get("fromId"),userRequestJson);
                }else{
                    this.simpMessagingTemplate.convertAndSend("/socket-publisher",userRequestJson);
                }
            }





            return new ResponseEntity<>(userRequestJson, new HttpHeaders(), HttpStatus.OK);

        } catch (Exception e) {
            message = "FAIL to upload " + file.getOriginalFilename() + "!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        }
    }

    @GetMapping(path = "/files/{filename}")
    @ResponseBody
    public ResponseEntity<Resource> getFile(@PathVariable String filename, HttpServletRequest request) {
        Resource file = storageService.loadFile(filename);
        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(file.getFile().getAbsolutePath());
        } catch (IOException ex) {
            //logger.info("Could not determine file type.");
        }
        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .body(file);
    }

}
