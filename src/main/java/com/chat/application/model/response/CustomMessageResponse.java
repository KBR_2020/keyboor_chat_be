package com.chat.application.model.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
@AllArgsConstructor
public class CustomMessageResponse {

        String response;
        int status;

}
